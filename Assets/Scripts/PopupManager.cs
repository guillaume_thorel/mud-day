﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class PopupManager : MonoBehaviour 
{
    public enum Phase
    {
        DetectedBody,
        Tuto,
        Game
    }

    [SerializeField]
    GameManager gameManager;

    [SerializeField]
    BodySourceView sourceView;

    [SerializeField]
    List<CheckedPose> listPose;

    [SerializeField]
    Animator m_controller;

    [SerializeField]
    List<Image> listImagePose;

    [SerializeField]
    Text countDownText;

    [SerializeField]
    float countDown = 15;

    [SerializeField]
    float timeToContinueWithoutBody = 3;

    [SerializeField]
    Animator playerController;

    private Phase phase;

    private float timerNoBodyDetected;
    private bool noBodyDetected;
    private float countDownInit;

    void Init()
    {
        for (int i = 0; i < listPose.Count; i++)
        {
           listImagePose[i].color = Color.black;
           listPose[i].poseValidate = false;
        }

        countDown = countDownInit;
        phase = Phase.DetectedBody;
        playerController.SetBool("begin", false);
        m_controller.SetBool("open", false);
    }

    void Awake()
    {
        countDownInit = countDown;
    }

    IEnumerator DisplayBanner()
    {
        noBodyDetected = false;
        playerController.SetBool("begin", true);
        yield return new WaitForSeconds(1);
        m_controller.SetBool("open", true);
        phase = Phase.Tuto;
    }

    void Update()
    {
        if (!gameManager.gameIsStarted)
        {
            if (phase == Phase.DetectedBody)
            {
                if (sourceView._Bodies.Count != 0)
                {
                    StartCoroutine(DisplayBanner());
                }
            }

            else if (phase == Phase.Tuto)
            {
                countDown -= Time.deltaTime;
                countDownText.text = ((int)countDown).ToString();

                if (countDown <= 0)
                {
                    phase = Phase.Game;
                }

                for(int i=0; i < listPose.Count; i++)
                {
                    if ( listPose[i].poseValidate )
                    {
                        listImagePose[i].color = Color.green;
                    }
                }

                if (sourceView._Bodies.Count == 0)
                {
                    if (!noBodyDetected)
                    {
                        noBodyDetected = true;
                        timerNoBodyDetected = Time.time + timeToContinueWithoutBody;
                    }

                    if ( Time.time >= timerNoBodyDetected)
                    {
                        Init();
                    }
                }
            }
            else
            {
                m_controller.SetBool("open", false);
                playerController.SetBool("run", true);
                gameManager.gameIsStarted = true;
            }
        }
    }
}
