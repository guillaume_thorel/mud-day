﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Kinect = Windows.Kinect;

public class Player : MonoBehaviour
{
    [SerializeField]
    float timerToHoldPose;

    [SerializeField]
    List<string> listSentences;

    [SerializeField]
    string holdSentence;

    [SerializeField]
    string successSentence;

    [SerializeField]
    Toggle toggle;

    [SerializeField]
    Text textToggle;

    [SerializeField]
    Data data;

    [SerializeField]
    List<Sprite> listPose;

    [SerializeField]
    Image currentPose;

    [SerializeField]
    GameObject cubeRight;

    [SerializeField]
    GameObject cubeLeft;

    [SerializeField]
    GameObject tutoPartII;

    [SerializeField]
    GameObject windowTextInfo;

    [SerializeField]
    GameObject obCanvasTutoII;

    [SerializeField]
    int countToDisplayTerrain;

    private int countChallenge;
    private float timerToSuccess;
    private bool getTime;
    private bool successChallenge;
    private bool tutoFinished;

    private int countReachedForDisplayTerrain;

    [SerializeField]
    CheckedPose checkedPoseTutoI;

    [SerializeField]
    List<TypePoseObject> listPoseObject;

    void Awake()
    {
        successChallenge = false;
        countChallenge = 0;
        getTime = false;
        currentPose.sprite = listPose[countChallenge];
        tutoFinished = false;
        countReachedForDisplayTerrain = 0;
        checkedPoseTutoI.SetPose(listPoseObject[countChallenge]);
    }


    void Update()
    {
       // cubeLeft.transform.localPosition = posHandLeft;
       // cubeRight.transform.localPosition = posHandRight;

        if (!tutoFinished)
        {
            if (countChallenge == 0)
            {
                if (checkedPoseTutoI.IsOnPose())
                {
                    if (!getTime)
                    {
                        getTime = true;
                        textToggle.text = holdSentence;
                        timerToSuccess = Time.time + timerToHoldPose;
                    }

                    if (Time.time >= timerToSuccess)
                    {
                        PassToNextChallenge();
                    }
                }
                else
                {
                    if (!successChallenge)
                    {
                        getTime = false;
                        textToggle.text = listSentences[countChallenge];
                    }
                }
            }
            else if (countChallenge == 1)
            {
                if (checkedPoseTutoI.IsOnPose())
                {
                    if (!getTime)
                    {
                        getTime = true;
                        textToggle.text = holdSentence;
                        timerToSuccess = Time.time + timerToHoldPose;
                    }

                    if (Time.time >= timerToSuccess)
                    {
                        PassToNextChallenge();
                    }
                }
                else
                {
                    if (!successChallenge)
                    {
                        getTime = false;
                        textToggle.text = listSentences[countChallenge];
                    }
                }
            }
            else
            {
                if (checkedPoseTutoI.IsOnPose())
                {
                    if (!getTime)
                    {
                        getTime = true;
                        textToggle.text = holdSentence;
                        timerToSuccess = Time.time + timerToHoldPose;
                    }

                    if (Time.time >= timerToSuccess)
                    {
                        PassToNextChallenge();
                    }
                }
                else
                {
                    if (!successChallenge)
                    {
                        getTime = false;
                        textToggle.text = listSentences[countChallenge];
                    }
                }
            }
        }
    }

    private void PassToNextChallenge()
    {
        if (!successChallenge)
        {
            textToggle.text = successSentence;
            toggle.isOn = true;
            successChallenge = true;
            StartCoroutine(DisplayNextPose());

        }
    }

    private IEnumerator DisplayNextPose()
    {
        yield return new WaitForSeconds(2);
        countChallenge++;

        if (countChallenge > 2)
        {
            countChallenge = 0;
            countReachedForDisplayTerrain++;

            if ( countReachedForDisplayTerrain == countToDisplayTerrain)
            {
                tutoPartII.SetActive(true);
                windowTextInfo.SetActive(false);
                currentPose.gameObject.SetActive(false);
                tutoFinished = true;
                obCanvasTutoII.SetActive(true);
                checkedPoseTutoI.enabled = false;
            }
        }
        checkedPoseTutoI.SetPose(listPoseObject[countChallenge]);
        currentPose.sprite = listPose[countChallenge];
        textToggle.text = listSentences[countChallenge];
        toggle.isOn = false;
        successChallenge = false;

        if (countChallenge == 2)
        {
            currentPose.transform.localScale = new Vector3(-1.25f, 1.25f, 1);
        }
        else
        {
            currentPose.transform.localScale = new Vector3(1.25f, 1.25f, 1);
        }
    }
}
