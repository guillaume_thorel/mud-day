﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Data : MonoBehaviour 
{
    public Text textAngleElbowRight;

    public Text textAngleElbowLeft;

    public Text textAngleShoulderLeft;

    public Text textAngleShoulderRight;
}
