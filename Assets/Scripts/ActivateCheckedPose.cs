﻿using UnityEngine;
using System.Collections;

public class ActivateCheckedPose : MonoBehaviour
{
    [SerializeField]
    LayerMask layerPlayer;

    [SerializeField]
    CheckedPose m_checkedPose;

    bool poseSucceded;

    AgentManager m_agent;

    string poseNotSuccess = "Désolé vous n'avez pas récupéré de bouteille !";
    string poseSuccess = "Bravo vous avez récupéré une bouteille !";

    void Awake()
    {
        poseSucceded = false;
    }

    void Start()
    {
    }


    void OnTriggerEnter(Collider other)
    {

        if ((1 << other.gameObject.layer & layerPlayer) != 0)
        {
            m_checkedPose.enabled = true;
          //  Debug.Log("ETNERJDFHJDSHFJS");
            m_agent = other.GetComponentInParent<AgentManager>();
        }
    }

    void OnTriggerStay(Collider other)
    {
        if (m_checkedPose.poseValidate)
        {
            //poseSucceded = true;
            //m_agent.UpdateText(poseSuccess);
            Popup.Instance().SetColor(Color.green);
           // Popup.Instance().Close();
            m_checkedPose.enabled = false;
            //gameObject.SetActive(false);
        }
    }

    void OnTriggerExit(Collider other)
    {
        if ((1 << other.gameObject.layer & layerPlayer) != 0)
        {
            if (!m_checkedPose.poseValidate)
            {
                Popup.Instance().SetColor(Color.red);
            }
            m_checkedPose.enabled = false;
            //gameObject.SetActive(false);
        }
    }
}
