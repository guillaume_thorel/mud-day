﻿using UnityEngine;
using System.Collections;
using Windows.Kinect;

public class ColorSourceView : MonoBehaviour
{
    [SerializeField]
    ColorSourceManager _ColorManager;

    [SerializeField]
    Material m_material;
    
    void Start ()
    {
        m_material.SetTextureScale("_MainTex", new Vector2(-1, 1));
    }
    
    void Update()
    {
        if (_ColorManager == null)
        {
            return;
        }

        m_material.mainTexture = _ColorManager.GetColorTexture();
    }
}
