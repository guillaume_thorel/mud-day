﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class POI : MonoBehaviour 
{
    enum TYPE
    {
        INFO_PREPARE,
        INFO_TAKE,
    }

    [SerializeField]
    LayerMask layerPlayer;

    [SerializeField]
    bool takeWithRightArm;

    [SerializeField]
    TYPE info;

    public List<string> listSentences;
   

    void Start()
    {
    }

    public void OnTriggerEnter(Collider other)
    {
        Debug.Log("enter");

        if ((1 << other.gameObject.layer & layerPlayer) != 0)
        {
            AgentManager m_agent = other.GetComponentInParent<AgentManager>();
            Debug.Log("here");
            if (info == TYPE.INFO_PREPARE)
            {
                m_agent.UpdateText(listSentences[0]);
            }
            else if (info == TYPE.INFO_TAKE)
            {
                if (!takeWithRightArm)
                    m_agent.UpdateText(listSentences[1]);
                else
                    m_agent.UpdateText(listSentences[2]);
            }
        }
    }
}
