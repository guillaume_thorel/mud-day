﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Popup : MonoBehaviour 
{
    public static Popup _instance;

    [SerializeField]
    Image imagePose;

    [SerializeField]
    Animator m_controller;

    public void OpenPopup(Sprite _sprite, bool _isLeftArm)
    {
        m_controller.SetBool("open", true);
        imagePose.sprite = _sprite;

        if (_isLeftArm)
            imagePose.rectTransform.localScale = new Vector3(-1, imagePose.rectTransform.localScale.y, imagePose.rectTransform.localScale.z);
        else
            imagePose.rectTransform.localScale = new Vector3(1, imagePose.rectTransform.localScale.y, imagePose.rectTransform.localScale.z);

        imagePose.color = new Color(imagePose.color.r, imagePose.color.g, imagePose.color.b, 0.5f);
    }


    private IEnumerator ClosePopup()
    {
        yield return new WaitForSeconds(1.5f);
        m_controller.SetBool("open", false);
    }

    public void Close()
    {
        Debug.Log("close");
        StartCoroutine(ClosePopup());
    }

    public void SetColor(Color _color)
    {
        imagePose.color = _color;
    }

    public static Popup Instance()
    {
        return _instance;
    }

    void Awake()
    {
        _instance = this;
    }
}
