﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Kinect = Windows.Kinect;


[System.Serializable]
public class AngleData
{
    public Kinect.JointType jointA;
    public Kinect.JointType jointB;
    public Kinect.JointType jointC;
    public Kinect.JointType jointD;
}