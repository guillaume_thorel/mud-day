﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Kinect = Windows.Kinect;


public class PlayerBody
{
    public Dictionary<Kinect.JointType, Vector3> joint;

    public GameObject bodyObject;

    public PlayerBody()
    {
        joint = new Dictionary<Kinect.JointType, Vector3>();
    }

    public void SetJoint(Kinect.JointType _typeJt, Vector3 pos)
    {
        if (!joint.ContainsKey(_typeJt))
        {
            joint.Add(_typeJt, pos);
        }
        else
        {
            joint[_typeJt] = pos;
        }
    }
} 
