﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CheckedPose : MonoBehaviour
{
    [SerializeField]
    TypePoseObject objectPose;

    [SerializeField]
    BodySourceView body;

    [SerializeField]
    bool useVectorUp;

    public bool poseValidate;

    [SerializeField]
    float timeForValidatePose = 0.25f;

    private float timer;
    private bool setTime;

    void Start()
    {
        poseValidate = false;
        setTime = false;
    }

    int count = 0;

    public void SetPose(TypePoseObject _pose)
    {
        objectPose = _pose;
    }

    public bool IsOnPose()
    {
        Vector3 _A = Vector3.zero;
        Vector3 _B = Vector3.zero;
       
        if (body._Bodies.Count == 0)
            return false;

        // parse all bodies
        foreach (KeyValuePair<ulong, PlayerBody> item in body._Bodies)
        {
            // boolean for testing at least, one person take the pose
            bool temp = true;
           // count = 0;

            // parse all angle
            foreach (var itemAngle in objectPose.Pose.listAngle)
            {
                // calculate vector between join A to join B
                _A = item.Value.joint[itemAngle.angleData.angle.jointA] - item.Value.joint[itemAngle.angleData.angle.jointB];

                // calculate vector between join C to join D
                _B = item.Value.joint[itemAngle.angleData.angle.jointC] - item.Value.joint[itemAngle.angleData.angle.jointD];

                float _fAngle = 0;
                // calculate angle

                if (!useVectorUp)
                    _fAngle = Vector3.Angle(_A, _B);
                else
                    _fAngle = Vector3.Angle(_A, Vector3.down);

                if (_fAngle < itemAngle.minAngle || _fAngle > itemAngle.maxAngle)
                {
                    temp = false;
                }

               /* if ( count == 0 )
                {
                    Debug.Log("angle base : " + _fAngle);
                }

                if (count == 1)
                {
                    Debug.Log("angle shoulder : " + _fAngle);
                }*/
                count++;
            }
            if (temp)
            {
                return true;
            }
        }
        return false;
    }

    public void Update()
    {
       if (!poseValidate && IsOnPose())
       {
         //  Debug.Log("On Pose : " + IsOnPose());

           if (!setTime)
           {
               setTime = true;
               timer = Time.time + timeForValidatePose;
           }

           if ( Time.time > timer)
           {
               poseValidate = true;
           }
       }
       else
       {
           setTime = false;
       }
      // Debug.Log("pose success : " + poseValidate);
    }

    void OnEnable()
    {
        poseValidate = false;
        setTime = false;
    }
}
