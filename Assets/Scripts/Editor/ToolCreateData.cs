﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

public static class ToolCreateData
{
    // add an item on the unity window for generate a list of players
    [MenuItem("MUD DAY/Create Pose")]
    public static void CreatePose()
    {
        TypePoseObject pose = ScriptableObject.CreateInstance<TypePoseObject>();
        pose = new TypePoseObject();
      /*  ListObjectPlayers _list = ScriptableObject.CreateInstance<ListObjectPlayers>();
        _list.listPlayers = new List<PlayerData>();*/

        AssetDatabase.CreateAsset(pose, "Assets/Resources/Pose.asset");
        AssetDatabase.SaveAssets();
    }

    [MenuItem("MUD DAY/Create Angle")]
    public static void CreateAngle()
    {
        AngleObject angle = ScriptableObject.CreateInstance<AngleObject>();
        angle = new AngleObject();
        /*  ListObjectPlayers _list = ScriptableObject.CreateInstance<ListObjectPlayers>();
          _list.listPlayers = new List<PlayerData>();*/

        AssetDatabase.CreateAsset(angle, "Assets/Resources/angle.asset");
        AssetDatabase.SaveAssets();
    }
}