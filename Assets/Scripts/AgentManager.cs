﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class AgentManager : MonoBehaviour
{
    public NavMeshAgent m_agent;

    [SerializeField]
    List<Transform> listTarget;

    [SerializeField]
    float destinationReachThreshold;

    public Animator m_controller;

    private int countTarget;
    private bool targetReached;

    private bool wait;

    [SerializeField]
    Text infoPlayer;

    void OnEnable()
    {
        m_agent.SetDestination(listTarget[countTarget].position);
        //m_controller.SetBool("wait", false);
     //   m_controller.applyRootMotion = true;
    }

    void Awake()
    {
        countTarget = 0;
        targetReached = false;
        wait = false;
    }

    void Update()
    {
        if (!wait)
        {
            float distance = Vector3.Distance(m_agent.destination, transform.position);

            if (distance <= destinationReachThreshold && !targetReached)
            {
                targetReached = true;
                countTarget++;
                if (countTarget >= listTarget.Count)
                {
                    countTarget = 0;
                }
                m_agent.SetDestination(listTarget[countTarget].position);
            }
            else
            {
                targetReached = false;
            }
        }
    }

    public void UpdateText(string _info)
    {
        infoPlayer.text = _info;
    }
}
