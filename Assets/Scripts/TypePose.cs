﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Kinect = Windows.Kinect;

[System.Serializable]
public class TypePose
{
    public enum Pose
    {
        TPOSE,
        RIGHT_ARM_EXTENDED,
        LEFT_ARM_EXTENDED,
        RIGHT_ARM_ESCALADE,
        LEFT_ARM_ESCALADE,
        DROP
    }

    [System.Serializable]
    public class CheckedAngle
    {
        public AngleObject angleData;
        public float minAngle;
        public float maxAngle;
    }

    public Pose type;

    public List<CheckedAngle> listAngle;
}
