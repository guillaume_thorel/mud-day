﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameManager : MonoBehaviour 
{
    [SerializeField]
    Animator fpsController;

    [SerializeField]
    AgentManager agentManager;

    public bool gameIsStarted;
    private bool gameLaunched;

    void Awake()
    {
        gameIsStarted = false;
        gameLaunched= false;
    }


    public void LaunchGame()
    {
        agentManager.enabled = true;
        fpsController.enabled = true;
    }

    void Update()
    {
        if ( !gameLaunched && gameIsStarted )
        {
            gameLaunched = true;
            LaunchGame();
        }
    }
}
