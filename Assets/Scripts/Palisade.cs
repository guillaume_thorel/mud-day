﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Palisade : MonoBehaviour
{
    [SerializeField]
    LayerMask layerPlayer;

    [SerializeField]
    Transform target;

    private bool posOk;
    private float timer;
    private float currentTimer;
    private int countPose;

    [SerializeField]
    float timerToSuccededPose;

    [SerializeField]
    List<CheckedPose> listPose;

    [SerializeField]
    Image palissadeImage;

    [SerializeField]
    Text textTimer;

    private bool needToCheckPose;
    private bool currentPoseSucceded;
    private int countPoseSucceeded;
    private bool isObstacleSucceeded;
    AgentManager _agent;

    void Awake()
    {
        posOk = false;
        needToCheckPose = false;
        countPose = 0;
        countPoseSucceeded = 0;
        currentPoseSucceded = false;
    }

    void OnTriggerEnter(Collider other)
    {
        if ((1 << other.gameObject.layer & layerPlayer) != 0)
        {
            _agent = other.GetComponentInParent<AgentManager>();
            _agent.m_controller.SetTrigger("palisade");
            timer = Time.time + 1;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if ((1 << other.gameObject.layer & layerPlayer) != 0)
        {
            other.GetComponentInParent<Animator>().SetBool("wait", false);
        }
    }

    void OnTriggerStay(Collider other)
    {
        if (!posOk && (1 << other.gameObject.layer & layerPlayer) != 0)
        {
            _agent.transform.position = target.position;

            if (Time.time > timer)
            {
                posOk = true;
                palissadeImage.gameObject.SetActive(true);
                listPose[countPose].enabled = true;
                needToCheckPose = true;
                currentTimer = Time.time + timerToSuccededPose;
            }
        }
    }

    void Update()
    {
        if (needToCheckPose)
        {
            textTimer.text = ((int)(currentTimer - Time.time)).ToString();

            if ((int)(currentTimer - Time.time) > 0)
            {
                if (listPose[countPose].poseValidate)
                {
                    needToCheckPose = false;
                    currentPoseSucceded = true;
                    countPoseSucceeded++;
                    Debug.Log("succeed");
                    _agent.m_controller.SetTrigger("success");
                    StartCoroutine(NextPose());
                }
            }
            else
            {
                needToCheckPose = false;
                currentPoseSucceded = false;
                Debug.Log("not succeded");
                StartCoroutine(NextPose());
            }
        }
    }

    IEnumerator NextPose()
    {
        listPose[countPose].enabled = false;
        yield return new WaitForSeconds(2);

        // if current is failed AND if we are on the second pose
        if (!currentPoseSucceded && (countPose == 1 || countPose == 2))
        {
            _agent.m_controller.SetTrigger("fail");
           //_agent.m_agent.Resume();
            this.enabled = false;
            Debug.Log("failed");
        }

        if (countPoseSucceeded == 2)
        {
           // _agent.m_agent.Resume();
            StopCoroutine(NextPose());
            this.enabled = false;
            Debug.Log("success");
        }

        countPose++;

        if (countPose < listPose.Count)
        {
            currentTimer = Time.time + timerToSuccededPose;
            needToCheckPose = true;
            listPose[countPose].enabled = true;
            currentPoseSucceded = false;
            Debug.Log("next psoe : " + countPose);
        }
    }
}
