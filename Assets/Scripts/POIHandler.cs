﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class POIHandler : MonoBehaviour
{
    enum TYPE
    {
        PREPARE,
        OBSTACLE
    };

    [SerializeField]
    TYPE type;

    [SerializeField]
    Sprite pose;

    [SerializeField]
    LayerMask layerPlayer;

    [SerializeField]
    bool isLeftArm;


    void OnTriggerEnter(Collider other)
    {
        if ((1 << other.gameObject.layer & layerPlayer) != 0)
        {
            AgentManager _agent = other.GetComponentInParent<AgentManager>();

            if (type == TYPE.PREPARE)
            {
                _agent.m_agent.speed = _agent.m_agent.speed / 1.5f;
                other.GetComponentInParent<Animator>().speed = 0.75f;
                Popup.Instance().OpenPopup(pose, isLeftArm);
            }
            else
            {
                Popup.Instance().SetColor(Color.black);
                Debug.Log("here");
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (type == TYPE.OBSTACLE)
        {

            Debug.Log("quit");

            if ((1 << other.gameObject.layer & layerPlayer) != 0)
            {
                Debug.Log("quit");
                AgentManager _agent = other.GetComponentInParent<AgentManager>();
                _agent.m_agent.speed = _agent.m_agent.speed * 1.5f;

                other.GetComponentInParent<Animator>().speed = 1f;
                Popup.Instance().Close();
            }
        }
    }
}
